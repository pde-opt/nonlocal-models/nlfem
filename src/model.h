/**
    Contains kernel and forcing functions which are used in the assembly. Different additional kernels can be added
    here.

   All kernel functions share the same function signature.
   
   @param x Physical point of the outer integration region.
   @param bT outer triangle.
   @param y Physical point of the inner integration region.
   @param aT inner triangle.
   @param mesh Mesh
   @param kernel_val Value of the the kernel. Pointer to double in case of diffusion. Pointer to a array
   of shape d x d in case of peridynamics.
   
   Similarly, all forcing terms share a singature.
   
   @param x Physical point of the integration region.
   @param forcing_out Pointer to a storage for the output value.

    @file model.cpp
    @author Manuel Klar
**/

#ifndef NONLOCAL_ASSEMBLY_MODEL_H
#define NONLOCAL_ASSEMBLY_MODEL_H

#include <cmath>
#include "MeshTypes.h"

// Pointer -------------------------------------------------------------------------------------------------------------
//// Kernel pointer and implementations
//TODO Use lambdas instead (check how to obtain inlining! Function pointer can never be inlined...)
//TODO change to template interface such that Kernel class with operator() which is a TwoPointFunction depending on Points
//TODO kernel is a class containing kernelFunction (TwoPointFunction) and truncation (TwoPointFunction, delta (TwoPointFunction)).
// The kernel knows its own delta, and this might be a function!
extern void (*model_kernel)(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh, double * kernel_val);
//TODO Default initialization of operator()...
void ERROR_wrongAccess(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh, double * kernel_val);

/**
 * Constant kernel in 2D case with Euclidean truncation. The constant is chosen such that the operator is equivalent to the Laplacian for
 * polynomials of degree less or equal to 2.
 */
void kernel_constant(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh,
                     double * kernel_val);
/**
 * This kernel models a convection diffusion setting for the L-2 ball in 2d.
 */
void kernel_convdiff(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh,
                     double * kernel_val);
/**
 * This constant kernel \f$\gamma(x,y) = c\Theta(x,y)\f$ models a diffusion setting for the L-2 ball in 2d and can evaluate varying coefficients Theta(aT, bT) (which are constant on an element T).
 */
void kernel_theta(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                     double *kernel_val);
/**
 * This constant kernel \f$\gamma(x,y) = c (1-\Theta(x,y))\f$ models a diffusion setting for the L-2 ball in 2d and can evaluate varying coefficients (1 - Theta(aT, bT)) (which are constant on an element T).
 */
void kernel_sparsetheta(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
double *kernel_val);

/**
 * This kernel models a convection diffusion setting for the L-infinity ball.
 */
void kernel_convdiffLinf2D(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh,
                     double * kernel_val);
/**
 * Constant kernel in 2D case with \f\ell_\infty\f truncation. The constant is chosen such that the operator is equivalent
 * to the laplacian for polynomials of degree less or equal to 2.
 */
void kernel_constantLinf2D(const double *x,  const ElementType &aT, const double *y,  const ElementType &bT, const MeshType &mesh,
                     double *kernel_val);

/**
 * Constant kernel in 2D case which truncates itself.
 * This means that the kernel returns 0, if \f$ |x-y| > \delta \f$. It is used to simulate a specific ball approximation.
 * The constant is chosen such that the operator is equivalent to the laplacian for
 * polynomials of degree less or equal to 2.
 */
void kernel_constantTruncated(const double *x,  const ElementType &aT, const double *y,  const ElementType &bT, const MeshType &mesh,
                              double *kernel_val);
void kernel_parabola(const double *x,  const ElementType &aT, const double *y,  const ElementType &bT, const MeshType &mesh,
                     double *kernel_val);
/**
 * Constant kernel in 1D. The constant is chosen such that the operator is equivalent to the laplacian for
 * polynomials of degree less or equal to 2.
 */
void kernel_constant1D(const double *x,  const ElementType &aT, const double *y,  const ElementType &bT, const MeshType &mesh,
                       double *kernel_val);
/**
 * 1D convection-diffusion kernel. \f$ \gamma(x,y) = c_\delta  (1 + \frac{(x-y)}{2})\f$,
 * where \f$c_\delta = \frac{3}{2\delta}\f$.
 */
void kernel_convdiff1D(const double *x,  const ElementType &aT, const double *y,  const ElementType &bT, const MeshType &mesh,
                       double *kernel_val);

void kernel_antisymmetric1D(const double *x,  const ElementType &aT, const double *y,  const ElementType &bT, const MeshType &mesh,
                       double *kernel_val);
/**
 * Constant kernel in 3D truncated by the Euclidean ball. The constant is chosen such that the operator is equivalent to the laplacian for
 * polynomials of degree less or equal to 2.
 */
void kernel_constant3D(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh,
                       double * kernel_val);
void kernel_labeled(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh,
                    double * kernel_val);
void kernel_notch(const double * x,  const ElementType &aT, const double * y,  const ElementType &bT, const MeshType &mesh,
                  double * kernel_val);
void kernel_labeledNotch(const double * x,  const ElementType &aT, const double * y,  const ElementType &bT, const MeshType &mesh,
                         double * kernel_val);
void kernel_labeledValve(const double * x,  const ElementType &aT, const double * y,  const ElementType &bT, const MeshType &mesh,
                         double * kernel_val);
/**
 * Kernel for peridynamics diffusion model. The scalar valued, weakly singular kernel reads as
 *
 *  \f[
 * \gamma(x,y) = \frac{1}{\| x - y \|}  \frac{3}{\pi \delta ^3}.
 *  \f]
 * The constant is chosen such that the operator is equivalent to the Laplacian for
 * polynomials of degree less or equal to 2.
 */
void kernel_linearPrototypeMicroelastic(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                           const MeshType &mesh, double * kernel_val);

/**
 * Kernel for truncated fractional-type operator. The scalar valued singular kernel reads as
 *
 *  \f[
 * \gamma(x,y) = \frac{1}{\| x - y \|^{d+2s} }\frac{2-2s}{\pi \delta^{2-2s}}.
 *  \f]
 * The constant is chosen such that the operator is equivalent to the Laplacian for
 * polynomials of degree less or equal to 2. The degree of the singularity depends on the choice of \f s \in (0,1)\f
 */
void kernel_fractional(const double * x,  const ElementType &aT, const double * y,  const ElementType &bT,
                         const MeshType &mesh, double * kernel_val);
/**
 * Kernel for the linear peridynamics model in 2D. The matrix valued weakly singular kernel reads as
 *
 *  \f[
 * \gamma(x,y) = (x-y) \otimes (x-y) \frac{1}{\| x - y \|^3}  \frac{12}{\pi \delta ^3}.
 *  \f]
 *
 * The constant is chosen such that the operator is equivalent to linear elasticity for
 * polynomials of degree less or equal to 2 and lame paramter
 * \f$\mu = \lambda = \frac{\pi}{4}\f$.
 */
void kernelField_linearPrototypeMicroelastic(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                                const MeshType &mesh, double * kernel_val);
/**
 * Kernel for the peridynamics model in 3D. The matrix valued weakly singular kernel reads as
 *
 *  \f[
 * \gamma(x,y) = (x-y) \otimes (x-y) \frac{1}{\| x - y \|^3}  \frac{3}{\delta ^3}.
 *  \f]
 */
void kernelField_linearPrototypeMicroelastic3D(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                               const MeshType &mesh, double * kernel_val);
void kernelField_constant(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                             const MeshType &mesh, double * kernel_val);

extern void (*model_f)(const double * x, const MeshType &mesh, double * forcing_out);
void ERROR_wrongAccess(const double * x, const MeshType &mesh, double * forcing_out);
void f_constant(const double * x, const MeshType &mesh, double * forcing_out);
/**
 * Linear vector valued forcing function \f$f = (1 + 2 x_1, x_2 ) \frac{\pi}{2} \f$. This
 * function is used for checking rates. The corresponding solution reads as
 * \f[
 *      u(x) = (x_2^2, x_1^2 x_2) \frac{2}{5}.
 * \f]
 * @param x
 * @param forcing_out
 */
void fField_linear(const double * x, const MeshType &mesh, double * forcing_out);
void fField_linear3D(const double * x, const MeshType &mesh, double * forcing_out);
void fField_constantRight(const double * x, const MeshType &mesh, double * forcing_out);
void fField_constantDown(const double * x, const MeshType &mesh, double * forcing_out);
void fField_constantBoth(const double * x, const MeshType &mesh, double * forcing_out);
/**
 * Linear scalar valued forcing function \f$f = -2 (x_1 + 1)\f$. Used for computation of rates.
 * @param x
 * @param forcing_out
 */
void f_linear(const double * x, const MeshType &mesh, double * forcing_out);
void f_gaussian(const double * x, const MeshType &mesh, double * forcing_out);
void f_jump(const double * x, const MeshType &mesh, double * forcing_out);
void f_tensorsin(const double * x, const MeshType &mesh, double * forcing_out);
void f_convdiff(const double * x, const MeshType &mesh, double * forcing_out);
void f_linear3D(const double * x, const MeshType &mesh, double * forcing_out);
void f_linear1D(const double * x, const MeshType &mesh, double * forcing_out);
void f_convdiff1D(const double * x, const MeshType &mesh, double * forcing_out);
void f_quadraticII1D(const double *x, const MeshType &mesh, double * forcing_out);
void f_quadratic1D(const double *x, const MeshType &mesh, double * forcing_out);
#endif //NONLOCAL_ASSEMBLY_MODEL_H
