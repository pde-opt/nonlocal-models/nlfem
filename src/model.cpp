#include <iostream>

#include "model.h"
#include "mathhelpers.h"

using namespace std;

// ### KERNEL ##########################################################################################################
void (*model_kernel)(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh, double * kernel_val);
void ERROR_wrongAccess(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh, double * kernel_val){
    //TODO cerr oder throw...
    cout << "ERROR in model.cpp/ERROR_wrongAccess(): You chose no kernel, but the assembly routine tried to call it." << endl;
    cout << "The names of possible choices are given in lookup_configuration() in Cassemble.cpp." << endl;
    abort();
};
// Implementations -----------------------------------------------------------------------------------------------------
void kernel_constant(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                     double *kernel_val) {
    *kernel_val = 4 / (M_PI * pow(mesh.sqdelta, 2));
}

void kernel_theta(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                     double *kernel_val) {
    const double c = 4 / (M_PI * pow(mesh.sqdelta, 2));
    const long aTdx = aT.Tdx;
    const long bTdx = bT.Tdx;
    const double Theta_ab = evaluateCSRentry(aTdx, bTdx, mesh.ptrTheta_indices, mesh.ptrTheta_indptr, mesh.ptrTheta_data);
    *kernel_val = c*Theta_ab;
}

void kernel_sparsetheta(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                     double *kernel_val) {
    const double c = 4 / (M_PI * pow(mesh.sqdelta, 2));
    const long aTdx = aT.Tdx;
    const long bTdx = bT.Tdx;
    const double Theta_ab = evaluateCSRentry(aTdx, bTdx, mesh.ptrTheta_indices, mesh.ptrTheta_indptr, mesh.ptrTheta_data);
    *kernel_val = c*(1.-Theta_ab);
}

void kernel_convdiff(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh,
                           double * kernel_val){
    const double c = 4 / (M_PI * pow(mesh.sqdelta, 2));
    const double diffusion = 1.e-3;
    const double b[2] = {1., 1.};
    const double convection = (y[0]*b[0] + y[1]*b[1] - x[0]*b[0] - x[1]*b[1])*0.5;
    *kernel_val = c*(diffusion + convection);
}

void kernel_constantLinf2D(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                     double *kernel_val) {
    *kernel_val = 3 / (4 * pow(mesh.sqdelta, 2));
}

void kernel_convdiffLinf2D(const double * x, const ElementType &aT, const double * y, const ElementType &bT, const MeshType &mesh,
                           double * kernel_val){
    const double c = 3 / (4 * pow(mesh.sqdelta, 2));
    double diffusion = 1.e-3;
    if (mesh.nTheta) {
    	diffusion = evaluateCSRentry(0, 0, mesh.ptrTheta_indices, mesh.ptrTheta_indptr, mesh.ptrTheta_data);
    }
    const double b[2] = {1., 1.};
    const double convection = (y[0]*b[0] + y[1]*b[1] - x[0]*b[0] - x[1]*b[1])*0.5;
    *kernel_val = c*(diffusion + convection);
}

void kernel_constantTruncated(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                     double *kernel_val) {
    double z[2];
    z[0] = x[0] - y[0];
    z[1] = x[1] - y[1];
    bool doesInteract = (mesh.sqdelta > vec_dot(z,z,2) );
    *kernel_val = doesInteract * 4 / (M_PI * pow(mesh.sqdelta, 2));
}

void kernel_constant1D(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                     double *kernel_val) {
    *kernel_val = 3./(2. * pow(mesh.delta, 3));
}

void kernel_convdiff1D(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                       double *kernel_val) {
    const double c = 3./(2. * pow(mesh.delta, 3));
    const double convection = 0.5 * (y[0] - x[0]);
    const double diffusion = 1.e-3;
    *kernel_val = c*(diffusion + convection);
}

void kernel_constant3D(const double *x, const ElementType &aT, const double *y, const ElementType &bT, const MeshType &mesh,
                       double *kernel_val) {
    *kernel_val = 15 / (M_PI * 4 * pow(mesh.delta, 5));
}

void kernel_linearPrototypeMicroelastic(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                             const MeshType &mesh, double * kernel_val) {
    double z[2];
    z[0] = x[0] - y[0];
    z[1] = x[1] - y[1];
    const double denominator = 1.0/sqrt(vec_dot(z,z,2));
    const double c =  3.0/(M_PI * pow(mesh.delta,3));
    *kernel_val = c*denominator;
}

void kernel_fractional(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                        const MeshType &mesh, double * kernel_val) {
    const double exponent = mesh.dim+2.*mesh.fractional_s;
    const double constant_exponent = 2. - 2.*mesh.fractional_s;
    double z[mesh.dim];
    for (int i = 0; i < mesh.dim; i++){
        z[i] = x[i] - y[i];
    }
    const double denominator = 1.0/pow(sqrt(vec_dot(z,z,mesh.dim)), exponent);
    // Constant is wrong I guess..
    const double c =  (constant_exponent)/(M_PI*pow(mesh.delta, constant_exponent));
    *kernel_val = c*denominator;
}

void kernelField_linearPrototypeMicroelastic(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                        const MeshType &mesh, double * kernel_val) {
    double z[2];
    z[0] = x[0] - y[0];
    z[1] = x[1] - y[1];
    double denominator = 1.0/pow(sqrt(vec_dot(z,z,2)),3);
    //double f0 = 2*mesh.sqdelta - vec_dot(z,z,2); // Sign changing.
    //double c =  12.0/(M_PI * pow(mesh.delta,3));
    double c =  3.0/pow(mesh.delta,3);
    kernel_val[0] = c*denominator*z[0]*z[0] ;//+ f0;
    kernel_val[1] = c*denominator*z[0]*z[1];
    kernel_val[2] = c*denominator*z[1]*z[0];
    kernel_val[3] = c*denominator*z[1]*z[1] ;//+ f0;
}

void kernelField_linearPrototypeMicroelastic3D(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                               const MeshType &mesh, double * kernel_val)
{
    double z[3];
    z[0] = x[0] - y[0];
    z[1] = x[1] - y[1];
    z[2] = x[2] - y[2];
    double denominator = 1.0/pow(sqrt(vec_dot(z,z,3)),3);
    //double f0 = 2*mesh.sqdelta - vec_dot(z,z,2); // Sign changing.
    //double c =  12.0/(M_PI * pow(mesh.delta,3));
    double c =  3.0/pow(mesh.delta,3);
    kernel_val[0] = c*denominator*z[0]*z[0] ;//+ f0;
    kernel_val[1] = c*denominator*z[0]*z[1];
    kernel_val[2] = c*denominator*z[0]*z[2];

    kernel_val[3] = c*denominator*z[1]*z[0];
    kernel_val[4] = c*denominator*z[1]*z[1] ;//+ f0;
    kernel_val[5] = c*denominator*z[1]*z[2] ;//+ f0;

    kernel_val[6] = c*denominator*z[2]*z[0];
    kernel_val[7] = c*denominator*z[2]*z[1] ;//+ f0;
    kernel_val[8] = c*denominator*z[2]*z[2] ;//+ f0;

}

void kernelField_constant(const double * x, const ElementType &aT, const double * y, const ElementType &bT,
                                             const MeshType &mesh, double * kernel_val) {
    //double z[2];
    //z[0] = x[0] - y[0];
    //z[1] = x[1] - y[1];
    //*kernel_val = 1./sqrt(vec_dot(z,z,2));
    // KERNEL ORDER [ker (0,0), ker (0,1), ker (1,0), ker (1,1)]
    kernel_val[0] = 4. / (M_PI * pow(mesh.sqdelta, 2));
    kernel_val[1] = 0.0;
    kernel_val[2] = 4. / (M_PI * pow(mesh.sqdelta, 2));
    kernel_val[3] = 0.0;
}

// ### RIGHT HAND SIDE #################################################################################################
void (*model_f)(const double * x, const MeshType &mesh, double * forcing_out);
void ERROR_wrongAccess(const double * x, const MeshType &mesh, double * forcing_out){
    cout << "ERROR in model.cpp/ERROR_wrongAccess(): You chose no forcing term, but the assembly routine tried to call it." << endl;
    cout << "The names of possible choices are given in lookup_configuration() in Cassemble.cpp." << endl;
    abort();
};
// Implementations -----------------------------------------------------------------------------------------------------
void f_constant(const double * x, const MeshType &mesh, double * forcing_out){
    forcing_out[0] = 1.0;
}
void fField_linear(const double * x, const MeshType &mesh, double * forcing_out){
    //const double c = M_PI / 5.0;
    const double c = M_PI / 2.0;
    forcing_out[0] = -c*(1.0 + 2*x[0]);
    forcing_out[1] = -c*x[1];

}
void fField_linear3D(const double * x, const MeshType &mesh, double * forcing_out){
    //const double c = M_PI / 5.0;
    const double c = M_PI / 2.0;
    forcing_out[0] = -c*(1.0 + 2*x[0])*x[1]*x[1];
    forcing_out[1] = -c*x[1]*x[1];
    forcing_out[2] = -c*x[2]*x[1]*x[1];

}
void fField_constantRight(const double * x, const MeshType &mesh, double * forcing_out){
    forcing_out[0] = 1.0e-1;
    forcing_out[1] = 0.0;
}
void fField_constantDown(const double * x, const MeshType &mesh, double * forcing_out){
    //const double f1 = 0; //1e-3;
    //const double f2 = -1.*1e-3;
    const double c = - M_PI;
    forcing_out[0] = 0;
    forcing_out[1] = c*2;
}
void fField_constantBoth(const double * x, const MeshType &mesh, double * forcing_out){
    //const double f1 = .5*1e-3;
    //const double f2 = 3./2.*1e-3;
    //forcing_out[0] = f1;
    //forcing_out[1] = f2;

    const double c = - M_PI;
    forcing_out[0] = c;
    forcing_out[1] = c*2;
}
void f_linear(const double * x, const MeshType &mesh, double * forcing_out){
    *forcing_out = -2. * (x[1] + 1.);
}
void f_tensorsin(const double * x, const MeshType &mesh, double * forcing_out){
    *forcing_out = 32.*pow(M_PI,2)*sin(x[0] * M_PI * 4)*sin(x[1] * M_PI * 4);
}

void f_convdiff(const double * x, const MeshType &mesh, double * forcing_out){
    const double epsilon = 1.e-3;
    *forcing_out = -4.*epsilon + 2. * x[0] + 2. * x[1];
}

void f_linear1D(const double * x, const MeshType &mesh, double * forcing_out){
    *forcing_out = -2. * (x[0] + 1.);
}

void f_convdiff1D(const double * x, const MeshType &mesh, double * forcing_out){
    const double epsilon = 1.e-3;
    *forcing_out = -2.*epsilon + 2. * x[0];
}
void f_linear3D(const double * x, const MeshType &mesh, double * forcing_out){
    *forcing_out = -2. * (x[1] + 2.);
}
void f_quadraticII1D(const double *x, const MeshType &mesh, double * forcing_out){
    double sqx = x[0]*x[0];
    double cbx = sqx*x[0];

    if (x[0] < 1.){
        *forcing_out = 2.*cbx - 3.*sqx - 3.*x[0];
    } else if(x[0] > 4.){
        *forcing_out = 12*sqx - 2*cbx + 3*x[0] - 65;
    } else {
        *forcing_out = -2.;
    }
}
void f_quadratic1D(const double *x, const MeshType &mesh, double * forcing_out){
    double sqx = x[0]*x[0];
    double cbx = sqx*x[0];

    if (x[0] < 1.){
        *forcing_out = 2.*cbx - 3.*x[0] - 1.;
    } else if(x[0] > 4.){
        *forcing_out = -2*cbx + 15*sqx + 3*x[0] - 126;
    } else {
        *forcing_out = -2.;
    }
}

