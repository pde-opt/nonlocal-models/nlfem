#-*- coding:utf-8 -*-
#distutils: include_dirs = include

#cython: language_level=3
#cython: boundscheck=False, wraparound=False, cdivision=True

# Assembly routine
from libcpp.string cimport string
cimport Cassemble

import numpy as np
import time
from libc.math cimport pow
import scipy.sparse as sparse
cimport numpy as c_np
import datetime

def show_options():
    """
    Shows options for kernel, load and configuration settings. The kernel and forcing
    functions are imported directly from src/Cassemble.cpp.
    """
    def fillprint(text, totallen, fillval):
        nfill = max(totallen - len(text), 0)
        print(text, fillval*nfill)

    fillprint("# Forcing ", 50, "#")
    fillprint("- function ", 40, "-")
    Cassemble.show_forcing_options()

    fillprint("# Kernels ", 50, "#")
    fillprint("- function ", 40, "-")
    Cassemble.show_kernel_options()

    fillprint("# Configuration ", 50, "#")
    fillprint("- ansatz ", 40, "-")
    print("CG, DG")
    fillprint("- approxBalls:method ", 40, "-")
    Cassemble.show_integration_options()
    fillprint("- closeElements [opt for sing kers] ", 40, "-")
    print("weakFractional","fractional")

def get_empty_settings():
    """
    Returns empty dictionaries which can be filled with values. Possible options
    can be printed via show_options().
    """
    settings = {
        "kernel":
            {
                "function": None,
                "horizon": 0.0,
                "outputdim": None,
                "Theta": sparse.csr_matrix([[1.0]])
            },
        "load": {"function": None},
        "conf": {
               "savePath": None,
               "ansatz": None,
               "approxBalls": {
                    "method": None,
                    "isPlacePointOnCap": True,
                },
                "quadrature": {
                    "outer": {
                        "points": None,
                        "weights": None
                    },
                    "inner": {
                        "points": None,
                        "weights": None
                    },
                    "touchingElements": {
                         "method": None,
                         "ntensorGaussPoints": 0
                    }
                },
                "verbose": False
            }
        }
    return settings

def _timestamp():
    """
    Returns current timestamp as string.

    :return: string, format %m%d_%H-%M-%S
    """
    # Link to strftime Doc
    # http://strftime.org/
    return datetime.datetime.now().strftime("%m%d_%H-%M-%S")

def _read_arma_mat(path, is_verbose=False):
    """
    Read armadillo vector format from file.

    :param path: string, Path to file.
    :param is_verbose: bool, Verbose mode.
    :return: np.ndarray, Vector of double.
    """
    import numpy as np

    sizeof_double = 8

    f = open(path, "rb")
    # Read Armadillo header
    arma_header = f.readline()
    if arma_header != b'ARMA_MAT_BIN_FN008\n':
        raise ValueError("in _read_arma_mat(), input file is of wrong format.")
    # Get shape of sparse matrix
    arma_shape = f.readline()
    n_rows, n_cols = tuple([int(x) for x in arma_shape.decode("utf-8").split()])
    if is_verbose: print("Shape (", n_rows, ", ", n_cols, ")", sep="")
    # Raw binary of sparse Matrix in csc-format
    b_data = f.read()
    f.close()

    b_values = b_data[:sizeof_double * n_rows * n_cols]
    values = np.array(np.frombuffer(b_values)).reshape((n_rows, n_cols), order="F")
    if is_verbose: print("Values ", values)
    if is_verbose: print(values)

    return values

def _read_arma_spMat(path, is_verbose=False):
    """
    Read sparse Matrix in armadillo spMat format from file.

    :param path: string, Path to file.
    :param is_verbose: bool, Verbose mode.
    :return: scipy.csr_matrix, Matrix of double.
    """

    import scipy.sparse as sp
    import numpy as np

    sizeof_double = 8

    f = open(path, "rb")
    # Read Armadillo header
    arma_header = f.readline()
    if arma_header != b'ARMA_SPM_BIN_FN008\n':
        raise ValueError("in _read_arma_spMat(), input file is of wrong format.")
    # Get shape of sparse matrix
    arma_shape = f.readline()
    n_rows, n_cols, n_nonzero = tuple([int(x) for x in arma_shape.decode("utf-8").split()])
    if is_verbose: print("Shape (", n_rows, ", ", n_cols, ")", sep="")
    # Raw binary of sparse Matrix in csc-format
    b_data = f.read()
    b_values = b_data[:sizeof_double * n_nonzero]
    b_pointers = b_data[sizeof_double * n_nonzero:]
    f.close()

    values = np.frombuffer(b_values)
    if is_verbose: print("Values ", values)

    pointers = np.frombuffer(b_pointers, dtype=np.uint)
    row_index = pointers[:n_nonzero]
    if is_verbose: print("Row index", row_index)
    col_pointer = pointers[n_nonzero:]
    if is_verbose: print("Column pointer", col_pointer)

    A = sp.csc_matrix((values, row_index, col_pointer), shape=(n_rows, n_cols)).transpose()
    A = A.tocsr() # This is efficient, linearly in n_nonzeros.
    if is_verbose: print(A.todense())
    return A

def _remove_arma_tmp(path):
    import os
    os.remove(path)

# Tensor Gauss Quadrature
class _tensorgauss:
    def __init__(self, deg, dim=4):
        self.deg = deg
        self.dim = dim
        p, w = np.polynomial.legendre.leggauss(deg)
        #print("Length", len(p), "\np", p, "w", w)
        self.N = deg**dim

        self.weights = np.ones(deg**dim)
        self.points = np.zeros((deg**dim, dim))
        l = 0
        k = np.zeros(self.dim, dtype=int)

        point_coord = np.array([row.flatten() for row in np.meshgrid(*([np.arange(self.deg)]*self.dim),
        indexing = "ij")]).T
        for k, dxRow in enumerate(point_coord):
            self.points[k] = p[dxRow]
            for dx in dxRow:
                self.weights[k] *= w[dx]

def stiffnessMatrix(
        # Mesh information ------------------------------------
        mesh,
        kernel,
        configuration
    ):
    """ Computes the stiffness matrix corresponding to the nonlocal operator

    .. math::

      -\mathcal{L}(\mathbf{u})(\mathbf{x}) = 2 \int_{B_{\delta}(\mathbf{x}) \cap \widehat{\Omega}}(\mathbf{C}_\delta(\mathbf{x}, \mathbf{y})  \mathbf{u}(\mathbf{x}) - \mathbf{C}_\delta(\mathbf{y}, \mathbf{x})\mathbf{u}(\mathbf{y}))  d\mathbf{y},

    on a given mesh. The input parameters are expected to be dictionaries. Find more information on the expected content in the `examples/Test2D/conf/testConfFull.py`. This function is a wrapper for the function par_assemble.

    :param mesh: Dictionary containing the mesh information (i.e. the keys ``"elements"``, ``"elementLabels"``, ``"vertices"``, and ``"vertexLabels"``). The values in the arrays ``"elements"``, ``"elementLabels"`` and ``"vertexLabels"`` are expected to be of datatype ``numpy.int``. The values in ``"vertices"`` are supposed to be of type ``numpy.float64``. Elements in the domain have positive labels. Elements in the nonlocal Dirichlet boundary have negative labels. For this purpose it does not matter which positive or negative number is used, and the kernels can depend on the element labels. The labels of the elements should be consistent with the vertex labels. In the case of Discontinuous Galerkin this means that the signs of the element labels and corresponding vertex labels coincide. In case of Continuous Galerkin this means that elements with negative label have only vertices with negative label.

    :param kernel: The kernel is assumed to be a dictionary containing the keys ``"function"``, and ``"outputdim"``. The value of ``"function"`` is a string which contains the name of the kernel (e.g. "constant"). You find all available options in the function *src/Cassemble.cpp/lookup_configuration()*. The key ``"outputdim"`` describes the output dimension of the kernel function. In case of a scalar valued kernel this would be 1. In case of the kernel "linearPrototypeMicroelasticField" this would be 2 (find an example in *examples/Test2D/testConfFull.py*). Note, that the value of ``"outputdim"`` does not define the output dimension of the kernel, but *describes* it. The value has to be consistent with the definition of the corresponding kernel in *src/model.cpp*.

    :param configuration: Dictionary containing the configuration (find an example in *examples/Test2D/conf/testConfFull.py*).

    :return: Matrix A in scipy csr-sparse format of shape K x K where K = nVerts * outdim (Continuous Galerkin) or K = nElems * (dim+1) * outdim (Discontinuous Galerkin).
    """
    tmstmp = _timestamp()
    path_spAd = configuration.get("savePath", f"tmp_spAd_{tmstmp}")
    cdef string path_spAd_ = path_spAd.encode('UTF-8')

    assert len(mesh["elements"].shape) == 2, "elements has to be of shape (nE, dim+1)"
    cdef long dim = mesh["elements"].shape[1] - 1
    cdef long nE = mesh["elements"].shape[0]
    cdef long nE_Omega = np.sum(mesh["elementLabels"] > 0)

    cdef long[:] elements = mesh["elements"].flatten()
    cdef long[:] elementLabels = mesh["elementLabels"].flatten()

    assert len(mesh["vertices"].shape) == 2, "vertices has to be of shape (nV, dim)"
    assert mesh["vertices"].shape[1] == dim, "vertices has to be of shape (nV, dim) not"

    cdef long nV = mesh["vertices"].shape[0]
    cdef long nV_Omega = np.sum(mesh["vertexLabels"] > 0)
    cdef double[:] vertices = mesh["vertices"].flatten()
    cdef long[:] vertexLabels = mesh["vertexLabels"].flatten()

    cdef string model_kernel_ = kernel["function"].encode('UTF-8')
    cdef string integration_method_remote = configuration["approxBalls"]["method"].encode('UTF-8')
    cdef string integration_method_touching = integration_method_remote
    integration_method_touching = configuration["quadrature"].get("touchingElements", {}).get("method", configuration["approxBalls"]["method"]).encode('UTF-8')

    cdef int is_PlacePointOnCap_ = configuration["approxBalls"].get("isPlacePointOnCap", 1)
    cdef int verbose = configuration.get("verbose", 0)

    assert dim == mesh["vertices"].shape[1], "Dimensions of vertices and number vertices per element do not match."

    cdef double [:] ptrPx = configuration["quadrature"]["outer"]["points"].flatten()
    cdef double [:] ptrPy = configuration["quadrature"]["inner"]["points"].flatten()
    cdef double [:] ptrdx = configuration["quadrature"]["outer"]["weights"].flatten()
    cdef double [:] ptrdy = configuration["quadrature"]["inner"]["weights"].flatten()
    cdef int nPx = configuration["quadrature"]["outer"]["weights"].shape[0]
    cdef int nPy = configuration["quadrature"]["inner"]["weights"].shape[0]

    assert len(configuration["quadrature"]["outer"]["points"].shape) == 2, "Quadrature points should be of shape (nPoints, dim)"
    assert len(configuration["quadrature"]["inner"]["points"].shape) == 2, "Quadrature points should be of shape (nPoints, dim)"
    assert configuration["quadrature"]["outer"]["points"].shape[1] == dim, "Quadrature rule does not match dimension"
    assert configuration["quadrature"]["inner"]["points"].shape[1] == dim, "Quadrature rule does not match dimension"

    cdef double [:] Pg
    cdef const double * ptrPg = NULL
    cdef double [:] dg
    cdef const double * ptrdg = NULL

    if (verbose): print("")

    ntensorGaussPoints = configuration["quadrature"].get("touchingElements", {}).get("ntensorGaussPoints", 0)
    if ntensorGaussPoints != 0:
        quadgauss = _tensorgauss(ntensorGaussPoints)
        Pg = quadgauss.points.flatten()
        ptrPg = &Pg[0]
        dg = quadgauss.weights.flatten()
        ptrdg = &dg[0]
    else:
        if (verbose): print("stiffnessMatrix(): ntensorGaussPoints not found or 0 (default).")

    cdef long [:] Theta_indices
    cdef long [:] Theta_indptr
    cdef double [:] Theta_data

    cdef long * ptrTheta_indices = NULL
    cdef long * ptrTheta_indptr = NULL
    cdef double * ptrTheta_data = NULL
    cdef long isTheta = 0

    isTheta = kernel.get("Theta", None) is not None
    if isTheta > 0:
        isTheta = kernel.get("nColsTheta", 1)
        Theta_indices = np.array(kernel["Theta"].indices, dtype=np.int64)
        Theta_indptr = np.array(kernel["Theta"].indptr, dtype=np.int64)
        Theta_data = np.array(kernel["Theta"].data, dtype=np.float_)
        ptrTheta_indices = &Theta_indices[0]
        ptrTheta_indptr = &Theta_indptr[0]
        ptrTheta_data = &Theta_data[0]
    else:
        if (verbose): print("stiffnessMatrix(): Theta not found.")

    cdef int is_fullConnectedComponentSearch = configuration.get("is_fullConnectedComponentSearch", 0)

    cdef int outdim_ = kernel["outputdim"]
    assert mesh["outdim"] == kernel["outputdim"], f"The output dimension of the mesh has to be equal to the output dimension of the kernel ({outdim_})."

    cdef double fractional_s  = kernel.get("fractional_s", -1.0)
    if verbose and not kernel.get("fractional_s", False): print("stiffnessMatrix(): fractional_s not found.")

    maxDiameter = mesh.get("diam", 0.0)
    if (verbose and (not maxDiameter)): print("stiffnessMatrix(): diam not found or 0.0 (default).")

    is_DG = configuration.get("ansatz", "CG") == "DG"
    if (verbose and (not maxDiameter)): print("stiffnessMatrix(): ansatz not found or CG (default).")

    cdef long K_Omega, K
    if is_DG:
        K = nE * (dim+1) * outdim_
        K_Omega = nE_Omega * (dim+1) * outdim_
    else:
        K = nV * outdim_
        K_Omega = nV_Omega * outdim_

    # Compute Assembly --------------------------------
    start = time.time()

    Cassemble.par_assemble( "system".encode('UTF-8'), path_spAd_,
                            "".encode('UTF-8'),
                            K_Omega, K,
                            &elements[0], &elementLabels[0], &vertices[0], &vertexLabels[0],
                            nE , nE_Omega,
                            nV, nV_Omega,
                            &ptrPx[0], nPx, &ptrdx[0],
                            &ptrPy[0], nPy, &ptrdy[0],
                            kernel["horizon"]**2,
                            is_DG,
                            0,
                            &model_kernel_[0],
                            "".encode('UTF-8'),
                            &integration_method_remote[0],
                            &integration_method_touching[0],
                            is_PlacePointOnCap_,
                            dim, outdim_,
                            ptrTheta_indices,
                            ptrTheta_indptr,
                            ptrTheta_data,
                            isTheta,
                            ptrPg, ntensorGaussPoints, ptrdg,
                            maxDiameter,
                            fractional_s,
                            is_fullConnectedComponentSearch,
                            verbose)

    total_time = time.time() - start
    if (verbose): print("stiffnessMatrix(): Assembly Time\t", "{:1.2e}".format(total_time), " Sec")

    Ad = _read_arma_spMat(path_spAd)
    if path_spAd == f"tmp_spAd_{tmstmp}":
        _remove_arma_tmp(path_spAd)
    return Ad

def loadVector(
        # Mesh information ------------------------------------
        mesh,
        load,
        configuration
    ):
    """
    Computes load vector.
    The input parameters are expected to be dictionaries. Find more information
    on the expected content in the example/Test2D/testConfFull.py

    :param mesh: Dictionary containing the mesh information (i.e. the keys ``"elements"``, ``"elementLabels"``, ``"vertices"``, ``"vertexLabels"`` and ``"outdim"``), where ``"outdim"`` is the output-dimension of the kernel. In this function the output dimension is read from ``"outdim"``, as no kernel is expected. The load cannot yet depend on label information.

    :param load: Dictionary containing the load information (find an example in testConfFull.py).
    :param configuration: Dictionary containing the configuration (find an example in *examples/Test2D/conf/testConfFull.py*).

    :return: Vector f of shape K = nVerts * outdim (Continuous Galerkin) or K = nElems * (dim+1) * outdim (Discontinuous Galerkin)
    """
    tmstmp = _timestamp()
    path_fd = configuration.get("savePath", f"tmp_fd_{tmstmp}")
    cdef string path_fd_ = path_fd.encode('UTF-8')

    #cdef long[:] neighbours = mesh["neighbours"].flatten()#nE*np.ones((nE*dVertex), dtype=int)
    #cdef int nNeighbours = mesh["neighbours"].shape[1]

    cdef long dim = mesh["elements"].shape[1] - 1
    cdef long nE = mesh["elements"].shape[0]
    cdef long nE_Omega = np.sum(mesh["elementLabels"] > 0)
    cdef long[:] elements = mesh["elements"].flatten()
    cdef long[:] elementLabels = mesh["elementLabels"].flatten()

    cdef long nV = mesh["vertices"].shape[0]
    cdef long nV_Omega = np.sum(mesh["vertexLabels"] > 0)
    cdef double[:] vertices = mesh["vertices"].flatten()
    cdef long[:] vertexLabels = mesh["vertexLabels"].flatten()

    cdef string model_load_ = load["function"].encode('UTF-8')
    cdef double sqdelta = load.get("horizon", 0.0) # Right hand side might depend on delta
    cdef string integration_method_remote = configuration["approxBalls"]["method"].encode('UTF-8')
    cdef string integration_method_touching = "".encode('UTF-8')
    cdef int is_PlacePointOnCap_ = configuration["approxBalls"].get("isPlacePointOnCap", 1)
    cdef int verbose = configuration.get("verbose", 0)

    cdef double [:] ptrPx = configuration["quadrature"]["outer"]["points"].flatten()
    cdef double [:] ptrdx = configuration["quadrature"]["outer"]["weights"].flatten()
    cdef int nPx = configuration["quadrature"]["outer"]["weights"].shape[0]


    cdef double [:] Pg
    cdef const double * ptrPg = NULL
    cdef double [:] dg
    cdef const double * ptrdg = NULL
    ntensorGaussPoints = 0

    cdef long [:] Theta_indices
    cdef long [:] Theta_indptr
    cdef double [:] Theta_data

    #cdef long * ptrTheta_data = NULL
    cdef long * ptrTheta_indices = NULL
    cdef long * ptrTheta_indptr = NULL
    cdef double * ptrTheta_data = NULL
    cdef long isTheta=0
    if (verbose): print("")

    isTheta = load.get("Theta", None) is not None
    if isTheta > 0:
        Theta_indices = np.array(load["Theta"].indices, dtype=np.int64)
        Theta_indptr = np.array(load["Theta"].indptr, dtype=np.int64)
        Theta_data = np.array(load["Theta"].data, dtype=np.float_)
        ptrTheta_indices = &Theta_indices[0]
        ptrTheta_indptr = &Theta_indptr[0]
        ptrTheta_data = &Theta_data[0]
    else:
        if (verbose): print("stiffnessMatrix(): Theta not found.")

    cdef int outdim_ = mesh["outdim"]

    is_DG = configuration.get("ansatz", "CG") == "DG"

    cdef long K_Omega, K
    if is_DG:
        K = nE * (dim+1) * outdim_
        K_Omega = nE_Omega * (dim+1) * outdim_
    else:
        K = nV * outdim_
        K_Omega = nV_Omega * outdim_

    # Compute Assembly --------------------------------
    start = time.time()
    Cassemble.par_assemble( "forcing".encode('UTF-8'), "".encode('UTF-8'),
                            path_fd_,
                            K_Omega, K,
                            &elements[0], &elementLabels[0],
                            &vertices[0], &vertexLabels[0],
                            nE, nE_Omega,
                            nV, nV_Omega,
                            &ptrPx[0], nPx, &ptrdx[0],
                            &ptrPx[0], nPx, &ptrdx[0],
                            sqdelta, is_DG, 0, "".encode('UTF-8'),
                            model_load_,
                            "".encode('UTF-8'), "".encode('UTF-8'), False,
                            dim, outdim_,
                            ptrTheta_indices,
                            ptrTheta_indptr,
                            ptrTheta_data,
                            isTheta,
                            NULL, 0, NULL, 0.0, -1.0, 0, verbose)

    total_time = time.time() - start
    if (verbose): print("loadVector(): Assembly Time\t", "{:1.2e}".format(total_time), " Sec")

    fd = _read_arma_mat(path_fd)[:,0]
    if path_fd == f"tmp_fd_{tmstmp}":
        _remove_arma_tmp(path_fd)
    return fd


def _get_vertexLabel(elements, elementLabels, vertices):
    nV = vertices.shape[0]
    vertexLabels = np.zeros(nV, dtype=np.int_)
    label_list = np.sort(np.unique(elementLabels))
    #print(label_list)
    for label in label_list[::-1]:
        if label:
            label_idx = np.where(elementLabels == label)
            vertex_idx = np.unique(elements[label_idx].ravel())
            vertexLabels[vertex_idx] = label
    return vertexLabels

def _get_diam(elements, vertices):
    dim = vertices.shape[1]
    nVerts = dim + 1
    diam = 0.0
    T = vertices[elements]
    for t in T:
        for k in range(nVerts):
            diff = t[k] - t[(k+1)%nVerts]
            dist = np.sqrt(diff**2)[0]
            if dist > diam:
                diam = dist
    return diam

def _meshFromArrays(elements, elementLabels, vertices, outputdim=1):
    vertexLabels = _get_vertexLabel(elements, elementLabels, vertices)
    diam = _get_diam(elements, vertices)

    mesh = {
        "dim": elements.shape[1] - 1,
        "nE": elements.shape[0],
        "nV": vertices.shape[0],
        "nE_Omega": np.sum(elementLabels > 0),
        "nV_Omega": np.sum(vertexLabels > 0),
        "elements": np.array(elements, dtype=np.int_),
        "elementLabels": elementLabels,
        "vertices": np.array(vertices),
        "vertexLabels": vertexLabels,
        "diam": diam,
        "outdim": outputdim
    }
    return mesh

def _setDofLabels(mesh, ansatz="CG", outputdim=1):
    mesh["ansatz"] = ansatz
    mesh["outdim"] = outputdim
    dim = mesh["dim"]
	
    if ansatz == "CG":
        mesh["dofLabels"] = np.repeat(mesh["vertexLabels"], outputdim)
        mesh["K"] = mesh["nV"]*outputdim
        mesh["K_Omega"] = mesh["nV_Omega"]*outputdim
    if ansatz == "DG":
        mesh["dofLabels"] = np.repeat(mesh["elementLabels"], (dim+1)*outputdim)
        mesh["K"] = (dim+1)*mesh["nE"]*outputdim
        mesh["K_Omega"] = (dim+1)*mesh["nE_Omega"]*outputdim


def stiffnessMatrix_fromArray(
        elements, elementLabels, vertices,
        kernel,
        configuration
    ):
    """ Computes the stiffness matrix corresponding to the nonlocal operator

    .. math::

      -\mathcal{L}(\mathbf{u})(\mathbf{x}) = 2 \int_{B_{\delta}(\mathbf{x}) \cap \widehat{\Omega}}(\mathbf{C}_\delta(\mathbf{x}, \mathbf{y})  \mathbf{u}(\mathbf{x}) - \mathbf{C}_\delta(\mathbf{y}, \mathbf{x})\mathbf{u}(\mathbf{y}))  d\mathbf{y},

    on a given mesh. This function is a wrapper around ``stiffnessMatrix()``.

    :param elements: Numpy array containing the information about the elements. The values in the array ``"elements"`` are expected to be of datatype ``numpy.int``.


    :param elementLabels: The values in the array ``"elementLabels"`` are expected to be of datatype ``numpy.int``. Elements in the domain have positive labels. Elements in the nonlocal Dirichlet boundary have negative labels. For this purpose it does not matter which positive or negative number is used, and the kernels can depend on the element labels. The routine does not compute contributions of elements with label zero, i.e. they are ignored. The label zero can therefore be added to connect disconnected domains. The labels of the vertices are automatically derived from the element labels. In the case of Discontinuous Galerkin this means that the signs of the element labels and corresponding vertex labels coincide. In case of Continuous Galerkin this means that a vertex gets the smallest label of the non-zero labels of the adjacent elements.

    :param vertices: The values in the array ``"vertices"`` are supposed to be of type ``numpy.float64``.

    :param kernel: The kernel is assumed to be a dictionary containing the keys ``"function"``, and ``"outputdim"``. The value of ``"function"`` is a string which contains the name of the kernel (e.g. "constant"). You find all available options in the function *src/Cassemble.cpp/lookup_configuration()*. The key ``"outputdim"`` describes the output dimension of the kernel function. In case of a scalar valued kernel this would be 1. In case of the kernel "linearPrototypeMicroelasticField" this would be 2 (find an example in *examples/Test2D/testConfFull.py*). Note, that the value of ``"outputdim"`` does not define the output dimension of the kernel, but *describes* it. The value has to be consistent with the definition of the corresponding kernel in *src/model.cpp*.

    :param configuration: Dictionary containing the configuration (find an example in *examples/test2D/conf/testConfFull.py*).

    :return mesh: Dictionary containing the mesh information. Can also be used for ``stiffnessMatrix()`` and ``loadVector()``.

    :return A: Matrix in scipy csr-sparse format of shape K x K where K = nVerts * outdim (Continuous Galerkin) or K = nElems * (dim+1) * outdim (Discontinuous Galerkin).
    """

    mesh = _meshFromArrays(elements, elementLabels, vertices, kernel["outputdim"])
    _setDofLabels(mesh, configuration["ansatz"], kernel["outputdim"])
    A = stiffnessMatrix(mesh, kernel, configuration)
    return mesh, A

def evaluateMass(
            mesh,
            ud,
            Px,
            dx
        ):
    vd = np.zeros(mesh.K)
    cdef double[:] ptrvd = vd
    cdef double[:] ptrud = ud.flatten()

    cdef long[:] elements = mesh.elements.flatten()
    cdef long [:] elementLabels = mesh.elementLabels.flatten()
    cdef double[:] vertices = mesh.vertices.flatten()
    cdef long [:] vertexLabels = mesh.vertexLabels.flatten()

    cdef double[:] ptrPx = Px.flatten()
    cdef double[:] ptrdx = dx.flatten()

    cdef int isDG = mesh.is_DiscontinuousGalerkin;

    cdef long outdim = 1
    try:
        outdim = mesh.outdim
    except AttributeError:
        pass

    Cassemble.par_evaluateMass(
            &ptrvd[0],
            &ptrud[0],
            &elements[0],
            &elementLabels[0],
            &vertices[0],
            &vertexLabels[0],
            mesh.K_Omega,
            mesh.nE_Omega,
            Px.shape[0], &ptrPx[0], &ptrdx[0], mesh.dim, outdim, isDG)
    return vd
