Documentation
=============

The implementation has a Python interface and a C++ core. The Python interface provides the main functionality. The C++ core documentation should provide sufficient insight to enable the user to extend nlfem, for example, by new kernel functions.

- `Python interface docs <http://pde-opt.gitlab-pages.uni-trier.de/nonlocal-models/nlfem/>`_
- `C++ core docs <http://pde-opt.gitlab-pages.uni-trier.de/nonlocal-models/nlfem/html/>`_

Build and Install
=================

Docker
--------
To quickly set up a terminal with an nlfem installation execute

::

    docker run --interactive --tty registry.gitlab.uni-trier.de/pde-opt/nonlocal-models/nlfem:latest bash

The above image contains a Jupyter notebook installation. You can start a notebook which can be reached
at ``http://localhost:<port>`` by

::

    docker run -p <port>:80 registry.gitlab.uni-trier.de/pde-opt/nonlocal-models/nlfem:latest jupyter notebook


Manual Installation
---------------------
Ubuntu 22.04 LTS
...................
On Ubuntu the required packages can be installed by the following commands:

::

  sudo apt-get update
  sudo apt-get install git gcc g++ libarmadillo-dev liblapack-dev libmetis-dev python3-venv python3-dev libgmp-dev libcgal-dev
  mkdir nlfemvenv
  python3 -m venv nlfemvenv/
  source nlfemvenv/bin/activate

The terminal will now indicate that you are in (nlfemvenv). In order to install nlfem run:

::

  git clone https://gitlab.uni-trier.de/pde-opt/nonlocal-models/nlfem.git
  cd nlfem
  python3 -m pip install -r requirements.txt
  python3 setup.py build install


Required packages
...................
In general a system has to meet the following requirements.

#.  The **basic requirements** are the programs ``gcc, g++, python3-dev, python3-venv, libgmp-dev, libcgal-dev, metis,`` ``libmetis-dev, libarmadillo-dev``. You further need the Python 3 packages ``numpy`` and ``scipy``. If you want to change the Cython code, you require the package ``Cython``.

#.  For **running the examples** you additionally need to install the Python packages ``matplotlib, meshzoo``. Make sure to install the correct versions by using the ``requirements.txt``.

License
=======

nlfem is published under GNU General Public License version 3. Copyright (c) 2021 Manuel Klar, Christian Vollmann

