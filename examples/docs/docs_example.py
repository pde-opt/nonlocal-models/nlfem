kernel = {
    "function": "fractional",
    "horizon": 0.1,
    "outputdim": 1,
    "fractional_s": 0.4
}
forcing = {"function": "linear"}

import numpy as np
Px = np.array([[0.33333333333333, 0.33333333333333],
               [0.47014206410511, 0.47014206410511],
               [0.47014206410511, 0.05971587178977],
               [0.05971587178977, 0.47014206410511],
               [0.10128650732346, 0.10128650732346],
               [0.10128650732346, 0.79742698535309],
               [0.79742698535309, 0.10128650732346]])
dx = 0.5 * np.array([0.22500000000000,
                     0.13239415278851,
                     0.13239415278851,
                     0.13239415278851,
                     0.12593918054483,
                     0.12593918054483,
                     0.12593918054483])
conf = {
    "ansatz": "CG",
    "approxBalls": {
        "method": "retriangulate"
    },
    "quadrature": {
        "outer": {
            "points": Px,
            "weights": dx
        },
        "inner": {
            "points": Px,
            "weights": dx
        },
        "touchingElements": {
            "method": "fractional",
            "ntensorGaussPoints": 3
        }
    },
    "verbose": True
}


def g(vertices):
    return vertices[:, 0] ** 2 * vertices[:, 1] + vertices[:, 1] ** 2

def options_table():
    import nlfem
    nlfem.show_options()

if __name__ == "__main__":
    import pickle
    import matplotlib.pyplot as plt

    mesh_arrays = pickle.load(open("mesh.pkl", "rb"))
    elements, vertices, elementLabels = mesh_arrays["elements"], mesh_arrays["vertices"], mesh_arrays["elementLabels"]

    plt.tripcolor(vertices[:, 0], vertices[:, 1], elements, facecolors=elementLabels, edgecolors="black", linewidth=.2, alpha=.7)
    plt.title(r"Domain $\Omega \cup \Gamma^D$")
    plt.gca().set_aspect('equal')
    plt.savefig("domain.png")
    plt.close()

    plt.plot([0, 0, 1, 0], [0, 1, 0, 0])
    plt.scatter(Px[:,0], Px[:,1])
    plt.savefig("quadrature.png")
    plt.close()

    import nlfem
    mesh, A = nlfem.stiffnessMatrix_fromArray(elements, elementLabels, vertices, kernel, conf)
    f = nlfem.loadVector(mesh, forcing, conf)

    Omega = mesh["dofLabels"] > 0
    Gamma = mesh["dofLabels"] < 0

    plt.triplot(vertices[:, 0], vertices[:, 1], elements, color="black", linewidth=.2, alpha=.7)
    plt.scatter(vertices[:, 0], vertices[:, 1], c=mesh["dofLabels"])
    plt.title(r"Degree of freedom labels on $\Omega \cup \Gamma$")
    plt.gca().set_aspect('equal')
    plt.savefig("doflabels.png")
    plt.close()

    from scipy.sparse.linalg import cg
    f_O = f[Omega]
    A_OO = A[Omega][:, Omega]
    A_OD = A[Omega][:, Gamma]
    g_D = g(vertices[Gamma])
    f_O -= A_OD.dot(g_D)

    u = np.zeros(vertices.shape[0], dtype=float)
    u[Omega] = cg(A_OO, f_O)[0]
    u[Gamma] = g_D

    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    surf = ax.plot_trisurf(vertices[:, 0],vertices[:, 1], u, cmap=plt.cm.Spectral)
    plt.savefig("solution.png")
    plt.close()

    options_table()
