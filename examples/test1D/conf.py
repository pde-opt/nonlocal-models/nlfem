import numpy as np

quadrules = {
    "1d": {
        "points": ((np.polynomial.legendre.leggauss(11)[0] + 1) / 2.).reshape(-1, 1),
        "weights": np.polynomial.legendre.leggauss(11)[1] / 2.
    }
}

cfg_dict = {
    "1d Neumann-type": {
        "kernel":
            {
                "function": "constant1D",
                "horizon": 1.,
                "outputdim": 1,
            },
        "load": {"function": "quadratic1D"},
        "conf":
            {
                "ansatz": "CG",  # DG
                "approxBalls": {
                    "method": "retriangulate1D",
                },
                "quadrature": {
                    "outer": {
                        "points": quadrules["1d"]["points"],
                        "weights": quadrules["1d"]["weights"]
                    },
                    "inner": {
                        "points": quadrules["1d"]["points"],
                        "weights": quadrules["1d"]["weights"]
                    }
                },
                "verbose": False
            },
        "boundarytype": "neumann"
    },
    "1d Diffusion": {
        "kernel":
            {
                "function": "constant1D",
                "horizon": 1.,
                "outputdim": 1,
            },
        "load":
            {"function": "constant",
             "factor": -2.
             },
        "conf":
            {
                "ansatz": "CG",
                "approxBalls": {
                    "method": "retriangulate1D_unsymm",
                },
                "quadrature": {
                    "outer": {
                        "points": quadrules["1d"]["points"],
                        "weights": quadrules["1d"]["weights"]
                    },
                    "inner": {
                        "points": quadrules["1d"]["points"],
                        "weights": quadrules["1d"]["weights"]
                    }
                },
                "verbose": False
            },
        "boundarytype": "dirichlet"
    },
    "1d Convection-Diffusion": {
        "kernel":
            {
                "function": "convdiff1D",
                "horizon": 1.,
                "outputdim": 1,
            },
        "load":
            {
                "function": "convdiff1D",
                "horizon": 1.
            },
        "conf":
            {
                "ansatz": "CG",  # DG
                "approxBalls": {
                    "method": "retriangulate1D_unsymm",
                },
                "quadrature": {
                    "outer": {
                        "points": quadrules["1d"]["points"],
                        "weights": quadrules["1d"]["weights"]
                    },
                    "inner": {
                        "points": quadrules["1d"]["points"],
                        "weights": quadrules["1d"]["weights"]
                    }
                },
                "verbose": False
            },
        "boundarytype": "dirichlet"
    }
}
