import nlfem
import numpy as np
import scipy.sparse.linalg as spl


def g_dirichlet(x):
    return x**2


class Mesh1d:
    def __init__(self, n, delta=1., ansatz="CG", boundarytype="neumann"):
        boundarysign = {"dirichlet": -1, "neumann": 1}[boundarytype]

        self.ansatz = ansatz
        self.is_DiscontinuousGalerkin = (ansatz == 'DG')

        m = int(n / 3)
        self.n = n
        self.m = m
        left = np.linspace(1 - delta, 1, m, endpoint=True)
        middle = np.linspace(1, 4, n, endpoint=True)
        right = np.linspace(4, 4 + delta, m)

        self.vertices = np.concatenate([left, middle, right]).reshape(-1, 1)
        self.elements = np.arange(0, len(self.vertices))
        self.elements = np.repeat(self.elements, 2)[1:-1]
        self.elements = self.elements.reshape(-1, 2)

        self.elementLabels = np.zeros(self.elements.shape[0], dtype=np.int_)
        self.elementLabels[:m] = 1 * boundarysign
        self.elementLabels[n + m:] = 1 * boundarysign
        self.elementLabels[m:n + m] = 2
        self.elementLabels[m - 1] = 0
        self.elementLabels[n + m - 1] = 0

        m_dict = nlfem._meshFromArrays(self.elements, self.elementLabels, self.vertices)
        nlfem._setDofLabels(m_dict, ansatz)

        self.K = m_dict["K"]
        self.K_Omega = m_dict["K_Omega"]  #
        self.nE = m_dict["nE"]
        self.nE_Omega = m_dict["nE_Omega"]
        self.vertexLabels = m_dict["vertexLabels"]
        self.solutionLabels = m_dict["dofLabels"]
        self.dim = m_dict["dim"]


class MassMatrix:
    def __init__(self, mesh_class):
        from conf import quadrules
        self.Px = quadrules['1d']['points']
        self.dx = quadrules['1d']['weights']
        self.mesh_class = mesh_class

        self.shape = (mesh_class.K_Omega, mesh_class.K_Omega)

    def matvec(self, x):
        x_ = np.zeros(self.mesh_class.K)
        x_[self.mesh_class.solutionLabels > 0] = x
        y_ = nlfem.evaluateMass(self.mesh_class, x_, self.Px, self.dx)
        return y_[self.mesh_class.solutionLabels > 0]


def rates1d(s=1):
    from conf import cfg_dict, quadrules
    N = 10 * 2 ** np.arange(s)
    N_fine = N[-1] * 8
    delta = 1.
    Px = quadrules['1d']['points']
    dx = quadrules['1d']['weights']
    ids = []
    for cfg_name in ["1d Neumann-type", "1d Diffusion", "1d Convection-Diffusion"]:
        cfg = cfg_dict[cfg_name]
        mesh_class_fine = Mesh1d(N_fine, delta, boundarytype=cfg["boundarytype"])
        err_ = None
        print(f"\n\n{cfg_name}")
        for n in N:
            mesh_class = Mesh1d(n, delta, boundarytype=cfg["boundarytype"])
            mesh_dict, A = nlfem.stiffnessMatrix_fromArray(mesh_class.elements,
                                                           mesh_class.elementLabels,
                                                           mesh_class.vertices,
                                                           cfg['kernel'],
                                                           cfg['conf'])
            Omega = mesh_dict["dofLabels"] > 0
            GammaD = mesh_dict["dofLabels"] < 0

            A_OO = A[Omega][:, Omega]
            A_OD = A[Omega][:, GammaD]
            f = nlfem.loadVector(mesh_dict, cfg['load'], cfg['conf'])*cfg['load'].get('factor', 1.)
            f_O = f[Omega]

            g = g_dirichlet(mesh_dict["vertices"])
            if cfg["conf"]["ansatz"] == 'DG':
                g = g[mesh_dict["elements"]].ravel()
            g = g[GammaD]
            f_D = (A_OD @ g).ravel()

            u = np.zeros(mesh_dict["dofLabels"].shape[0])
            u[Omega] = spl.gmres(A_OO, f_O - f_D, tol=1.e-11)[0]
            u[GammaD] = g.ravel()

            uf = np.interp(mesh_class_fine.vertices.ravel(), mesh_dict["vertices"].ravel(), u)
            u_exact = mesh_class_fine.vertices.ravel() ** 2
            if not np.any(GammaD):
                u_exact -= np.mean(u_exact)
                uf -= np.mean(uf)  # interpolation is not supposed to shift the function in total.
            errors = uf - u_exact
            Merr = nlfem.evaluateMass(mesh_class_fine, errors, Px, dx)
            err = errors.dot(Merr)
            err = np.sqrt(err)
            print("L2 Error: ", err)
            if err_ is not None and err_ > 1e-16:
                rate = np.log(err_ / err) / np.log(2)
                print("Rate: \t", rate)
            err_ = err
    return


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Run convergence test for the given configuration file.')
    parser.add_argument('-s', default=5, type=int, help='Number of steps of the convergence test.')
    args = parser.parse_args()
    s = int(args.s)
    rates1d(s)
