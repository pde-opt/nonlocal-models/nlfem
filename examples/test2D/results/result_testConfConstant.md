### Setting
| | |
| --- | --- |
| Ansatz space | CG |
| Right hand side | linear |
| **Kernel** | **constant** |
| Horizon $\delta$ | 0.1 |
| Fractional constant $s$ | - |
| **Intgr. remote pairs** | **retriangulate** |
| With caps | True |
| Quadrule outer element | 16 |
| Quadrule inner element | 16 |
| **Intgr. touching pairs**
(Relevant only if singular) | **-** |
| Singular quad degree | - |
### Rates
| $h$| $K_\Omega$| L2 Error| Rates| 
|---|---|---|---|
| 1.41e-01 | 1.60e+01 | 5.84e-04 | 0.00e+00 |
| 7.07e-02 | 8.10e+01 | 1.40e-04 | 2.06e+00 |
| 3.54e-02 | 3.61e+02 | 3.47e-05 | 2.01e+00 |
| 1.77e-02 | 1.52e+03 | 8.22e-06 | 2.08e+00 |
\newpage 
### Setting
| | |
| --- | --- |
| Ansatz space | CG |
| Right hand side | linear |
| **Kernel** | **constant** |
| Horizon $\delta$ | 0.1 |
| Fractional constant $s$ | - |
| **Intgr. remote pairs** | **retriangulate** |
| With caps | False |
| Quadrule outer element | 16 |
| Quadrule inner element | 16 |
| **Intgr. touching pairs**
(Relevant only if singular) | **-** |
| Singular quad degree | - |
### Rates
| $h$| $K_\Omega$| L2 Error| Rates| 
|---|---|---|---|
| 1.41e-01 | 1.60e+01 | 1.17e-03 | 0.00e+00 |
| 7.07e-02 | 8.10e+01 | 3.10e-04 | 1.92e+00 |
| 3.54e-02 | 3.61e+02 | 7.65e-05 | 2.02e+00 |
| 1.77e-02 | 1.52e+03 | 2.07e-05 | 1.88e+00 |
\newpage 
