import numpy as np

def u_exact_linearRhs(x):
    return x[0] ** 2 * x[1] + x[1] ** 2
def u_exact_FieldConstantBothRhs(x):
    return np.array([x[1]**2, x[0]**2 * x[1]])*0.4

KERNELS = [
    {
        "function": "constant",
        "horizon": 0.1,
        "outputdim": 1
    }
]

LOADS = [
    {"function": "linear", "solution": u_exact_linearRhs}
]

Px = np.array([[0.33333333, 0.33333333],
               [0.45929259, 0.45929259],
               [0.45929259, 0.08141482],
               [0.08141482, 0.45929259],
               [0.17056931, 0.17056931],
               [0.17056931, 0.65886138],
               [0.65886138, 0.17056931],
               [0.05054723, 0.05054723],
               [0.05054723, 0.89890554],
               [0.89890554, 0.05054723],
               [0.26311283, 0.72849239],
               [0.72849239, 0.00839478],
               [0.00839478, 0.26311283],
               [0.72849239, 0.26311283],
               [0.26311283, 0.00839478],
               [0.00839478, 0.72849239]])

dx = 0.5 * np.array([0.14431560767779
                        , 0.09509163426728
                        , 0.09509163426728
                        , 0.09509163426728
                        , 0.10321737053472
                        , 0.10321737053472
                        , 0.10321737053472
                        , 0.03245849762320
                        , 0.03245849762320
                        , 0.03245849762320
                        , 0.02723031417443
                        , 0.02723031417443
                        , 0.02723031417443
                        , 0.02723031417443
                        , 0.02723031417443
                        , 0.02723031417443])

Py = Px
dy = dx

CONFIGURATIONS = [
    {
        "ansatz": "CG",
        "is_fullConnectedComponentSearch": 0,
        "approxBalls": {
            "method": "retriangulate",
            "isPlacePointOnCap": True,
        },
        "quadrature": {
            "outer": {
                "points": Px,
                "weights": dx
            },
            "inner": {
                "points": Px,
                "weights": dx
            }
        },
        "verbose": False
    },
    {
        "ansatz": "CG",
        "is_fullConnectedComponentSearch": 0,
        "approxBalls": {
            "method": "retriangulate",
            "isPlacePointOnCap": False,
        },
        "quadrature": {
            "outer": {
                "points": Px,
                "weights": dx
            },
            "inner": {
                "points": Px,
                "weights": dx
            }
        },
        "verbose": False
    }
]