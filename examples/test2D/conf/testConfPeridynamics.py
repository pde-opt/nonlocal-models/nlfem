import numpy as np

def u_exact_linearRhs(x):
    return x[0] ** 2 * x[1] + x[1] ** 2

def u_exact_FieldConstantBothRhs(x):
    return np.array([x[1]**2, x[0]**2 * x[1]])#*0.4

KERNELS = [
    {
        "function": "linearPrototypeMicroelasticField",
        "horizon": 0.1,
        "outputdim": 2,
        "fractional_s": -0.5 
		# The kernel has singularity of degree 1, and 1 = 2+2s for s = -0.5. 
		# s cannot be interpreted as fractional coefficient here. 
    }
]

LOADS = [
    {"function": "linearField", "solution": u_exact_FieldConstantBothRhs}
]


Py = np.array([[0.33333333333333,    0.33333333333333],
                  [0.47014206410511,    0.47014206410511],
                  [0.47014206410511,    0.05971587178977],
                  [0.05971587178977,    0.47014206410511],
                  [0.10128650732346,    0.10128650732346],
                  [0.10128650732346,    0.79742698535309],
                  [0.79742698535309,    0.10128650732346]])
dy = 0.5 * np.array([0.22500000000000,
                        0.13239415278851,
                        0.13239415278851,
                        0.13239415278851,
                        0.12593918054483,
                        0.12593918054483,
                        0.12593918054483])

Px = np.array([[0.33333333, 0.33333333],
              [0.45929259, 0.45929259],
              [0.45929259, 0.08141482],
              [0.08141482, 0.45929259],
              [0.17056931, 0.17056931],
              [0.17056931, 0.65886138],
              [0.65886138, 0.17056931],
              [0.05054723, 0.05054723],
              [0.05054723, 0.89890554],
              [0.89890554, 0.05054723],
              [0.26311283, 0.72849239],
              [0.72849239, 0.00839478],
              [0.00839478, 0.26311283],
              [0.72849239, 0.26311283],
              [0.26311283, 0.00839478],
              [0.00839478, 0.72849239]])

dx =    0.5 * np.array([0.14431560767779
                       , 0.09509163426728
                       , 0.09509163426728
                       , 0.09509163426728
                       , 0.10321737053472
                       , 0.10321737053472
                       , 0.10321737053472
                       , 0.03245849762320
                       , 0.03245849762320
                       , 0.03245849762320
                       , 0.02723031417443
                       , 0.02723031417443
                       , 0.02723031417443
                       , 0.02723031417443
                       , 0.02723031417443
                       , 0.02723031417443])

CONFIGURATIONS = [
    {
        "ansatz": "DG",
        "approxBalls": {
            "method": "retriangulate",
            "isPlacePointOnCap": True
        },
        "quadrature": {
            "outer": {
                "points": Px,
                "weights": dx
            },
            "inner": {
                "points": Py,
                "weights": dy,
            },
	   		"touchingElements": {
            	 "method": "retriangulate"
            }
			
        },
        "verbose": True
    },
    {
        "ansatz": "CG",
        "approxBalls": {
            "method": "retriangulate",
            "isPlacePointOnCap": True
        },
        "quadrature": {
            "outer": {
                "points": Px,
                "weights": dx
            },
            "inner": {
                "points": Py,
                "weights": dy,
            },
            "touchingElements": {
            	 "method": "retriangulate"
            }
        },
        "verbose": True
    }
]
