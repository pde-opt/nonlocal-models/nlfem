import numpy as np

def u_exact_linearRhs(x):
    return x[0] ** 2 * x[1] + x[1] ** 2
def u_exact_FieldConstantBothRhs(x):
    return np.array([x[1]**2, x[0]**2 * x[1]])*0.4

KERNELS = [
    {
        "function": "constantLinf2D",
        "horizon": 1./10.,
        "outputdim": 1
    }
]

LOADS = [
    {"function": "linear", "solution": u_exact_linearRhs}
]


Px = np.array([[0.33333333333333,    0.33333333333333],
                  [0.47014206410511,    0.47014206410511],
                  [0.47014206410511,    0.05971587178977],
                  [0.05971587178977,    0.47014206410511],
                  [0.10128650732346,    0.10128650732346],
                  [0.10128650732346,    0.79742698535309],
                  [0.79742698535309,    0.10128650732346]])
dx = 0.5 * np.array([0.22500000000000,
                        0.13239415278851,
                        0.13239415278851,
                        0.13239415278851,
                        0.12593918054483,
                        0.12593918054483,
                        0.12593918054483])

Py = np.array([[0.47014206410511,    0.47014206410511],
                [0.47014206410511,    0.05971587178977],
                [0.05971587178977,    0.47014206410511]])
dy = np.array([1./3., 1./3., 1./3.]) * .5


CONFIGURATIONS = [
{
        "ansatz": "CG",
        "approxBalls": {
            "method": "retriangulate_unsymmLinfty"
        },
        "quadrature": {
            "outer": {
                "points": Px,
                "weights": dx
            },
            "inner": {
                "points": Py,
                "weights": dy,
            }
        }
    },
    {
        "ansatz": "DG",
        "approxBalls": {
            "method": "retriangulateLinfty"
        },
        "quadrature": {
            "outer": {
                "points": Px,
                "weights": dx
            },
            "inner": {
                "points": Px,
                "weights": dx
            }
        }
    }
]
